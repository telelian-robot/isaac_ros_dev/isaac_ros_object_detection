

## Face Net model 

```bash
./workspaces/isaac_ros-dev/src/isaac_ros_object_detection/isaac_ros_detectnet/scripts/setup_model.sh --height 720 --width 1280 --config-file resources/isaac_sim_config.pbtxt
```

## test

```bash 
 ros2 launch isaac_ros_detectnet dg_.launch.py 
 ```
